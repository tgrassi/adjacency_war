

class Language:

    # *****************
    def __init__(self):
        self.surnames = self.load_surnames()
        self.names = self.load_names()

        self.jobs = self.load_jobs()

    # *****************
    def get_job(self):
        from random import shuffle
        shuffle(self.jobs)
        return self.jobs[0]

    # *****************
    def get_name(self, country):
        from random import shuffle

        shuffle(self.names[country])
        shuffle(self.surnames[country])
        roles = ["generale", "caporale", "colonnello", "maresciallo"]

        shuffle(roles)

        return "%s %s %s" % (
                roles[0].capitalize(),
                self.names[country][0].title(),
                self.surnames[country][0].title())

    # *****************
    def get_name_list(self, country, number=50):
        from random import shuffle

        shuffle(self.names[country])
        shuffle(self.surnames[country])

        n_names = len(self.names[country])
        n_surnames = len(self.surnames[country])
        roles = ["generale", "caporale", "colonnello", "maresciallo"]

        name_list = []
        for i in range(number):
            shuffle(roles)
            name_list.append("%s %s %s" % (
                roles[0].capitalize(),
                self.names[country][i % n_names].capitalize(),
                self.surnames[country][i % n_surnames].capitalize()))

        return name_list

    # *****************
    @staticmethod
    def load_surnames(folder="data/names/surnames/"):
        import glob

        surnames = dict()
        for fname in glob.glob(folder + "*.dat"):
            cid = fname.replace(folder, "").replace(".dat", "")
            with open(fname) as f:
                surnames[cid] = [x for x in f.read().splitlines()]

        return surnames

    # *****************
    @staticmethod
    def load_names(folder="data/names/names/"):
        import glob

        names = dict()
        for fname in glob.glob(folder + "*.dat"):
            cid = fname.replace(folder, "").replace(".dat", "")
            with open(fname) as f:
                names[cid] = [x for x in f.read().splitlines()]

        return names

    # ******************
    @staticmethod
    def load_jobs(fname="data/jobs.dat"):
        with open(fname) as f:
            return [x for x in f.read().splitlines() if x != ""]

    # ******************
    def prepare_matrix(self):
        mapx = dict()

        for k, surnames in self.surnames.iteritems():
            mapx[k] = dict()
            for surname in surnames:
                cold = "#"
                for c in list(surname.lower() + "@"):
                    if cold not in mapx[k]:
                        mapx[k][cold] = []
                    mapx[k][cold].append(c)
                    cold = c

        return mapx

    # *******************
    def get_surname(self, country):
        from random import shuffle
        mapx = self.mapx[country]

        while True:
            c = "#"
            surname = ""
            voc_count = cons_count = 0
            ok = True
            for _ in range(100):
                shuffle(mapx[c])
                c = mapx[c][0]
                if c == "@":
                    break

                surname += c

                if self.is_voc(c):
                    voc_count += 1
                    cons_count = 0
                else:
                    voc_count = 0
                    cons_count += 1
                if voc_count > 2 or cons_count > 2:
                    ok = False

            if not ok:
                continue
            if len(surname) < 6 or len(surname) > 10:
                continue

            break

        print(surname)

    # *************
    @staticmethod
    def is_voc(arg):
        return arg in list("aeiouy")
