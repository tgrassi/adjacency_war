# merges countries in reps list from file_rep into file_base.
# merged file is file_merge

import json

file_base = "nuts2.geojson"
file_rep = "nuts1.geojson"
file_merge = "merged.geojson"

# countries to be replaced
reps = ["DE", "UK", "NL", "BE", "TR", "PL", "EL"]

# open geojson files
with open(file_base) as json_file:
    data_base = json.load(json_file)

with open(file_rep) as json_file:
    data_rep = json.load(json_file)


# init merged data
data_merge = {"type": data_base["type"], "features": []}

# loop on base to remove countries and from merged
for fea in data_base["features"]:
    if fea["properties"]["CNTR_CODE"] in reps:
        continue
    data_merge["features"].append(fea)

# loop on rep data to replace countries into merged
for fea in data_rep["features"]:
    if fea["properties"]["CNTR_CODE"] not in reps:
        continue
    data_merge["features"].append(fea)

# loop to count regions per country
count = dict()
for fea in data_merge["features"]:
    cc = fea["properties"]["CNTR_CODE"]
    if cc not in count:
        count[cc] = 0
    count[cc] += 1

# print regions per country
for k, v in count.iteritems():
    print k, v

# write merged to geojson file
with open(file_merge, "w") as json_file:
    json.dump(data_merge, json_file)

