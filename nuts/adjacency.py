import json
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import Delaunay
import itertools
from shapely.geometry import Polygon, Point, LineString
from shapely.ops import nearest_points


# open geojson files
with open("merged.geojson") as json_file:
    data = json.load(json_file)

coo_dict = dict()
polys = dict()
for fea in data["features"]:

    # get fid and geometry
    fid = fea["properties"]["FID"]
    geo = fea["geometry"]
    # if Polygon print in single shot
    if geo["type"] == "Polygon":
        coo = geo["coordinates"][0]
        xs, ys = zip(*coo)
        plt.fill(xs, ys)
        # average for pseudo-center
        xa = np.average(xs)
        ya = np.average(ys)
        # print pseudo-center
        plt.scatter([xa], [ya], color="k", zorder=99, s=2)
        # add polygon to dictionary
        polys[fid] = Polygon(coo)

    elif geo["type"] == "MultiPolygon":
        xall = []
        yall = []
        max_area = 0e0
        # loop on polygons
        for pol in geo["coordinates"]:
            xs, ys = zip(*pol[0])
            xall += xs
            yall += ys
            plt.fill(xs, ys)
            mypoly = Polygon(pol[0])
            area = mypoly.area
            # choose largest polygon as representative polygon
            if area >= max_area:
                polys[fid] = mypoly
                max_area = area

        # add pseudo-center
        xa = np.average(xall)
        ya = np.average(yall)
        plt.scatter([xa], [ya], color="k", zorder=99, s=2)

    # store pseudo-center
    coo_dict[fid] = [xa, ya]

# map of adjacency
adjacency = dict()

# loop on shapes
for fid, coo in coo_dict.iteritems():
    is_connected = False
    # loop on shapes
    for fid2, coo2 in coo_dict.iteritems():
        # skip itself
        if fid == fid2:
            continue

        # create line connecting centers
        line = LineString((coo, coo2))

        is_nei = True
        # loop on poly to check if its in between the two connecting polys
        for pid, poly in polys.iteritems():
            # skip the two polys
            if pid != fid and pid != fid2:
                its = line.crosses(poly)
                # if 3rd poly crossed by line, it means poly is in between the two connecting polys
                if its:
                    is_nei = False
                    break

        # when neibouring check if there is "see" in between
        if is_nei:
            # nearest poly points
            nps = nearest_points(polys[fid], polys[fid2])
            # get poly distance
            d = nps[0].distance(nps[1])
            # if close than limit connect
            if d < 3e0:
                # plot
                plt.plot([coo[0], coo2[0]], [coo[1], coo2[1]])
                # change flag to check if fid is connected to anyone
                is_connected = True
                # add to adjacency
                if fid not in adjacency:
                    adjacency[fid] = []
                adjacency[fid].append(fid2)

    # print name if shape fidn is not connected
    if not is_connected:
        print fid

plt.show()

# write merged to geojson file
with open("adj.json", "w") as json_file:
    json.dump(adjacency, json_file)


