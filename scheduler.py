

# ******************
# this class launches jobs to plot
# on a number of cores
class Scheduler:
    # ***************
    # init
    def __init__(self, cores=4):
        self.ncores = cores

    # ***************
    # plot
    def run(self):
        import glob
        import numpy as np
        import time
        from subprocess import Popen

        npickles = len(glob.glob("pickles/world_*.dat"))

        chunks = np.array_split(range(npickles), self.ncores)

        ps = []

        for ii in range(self.ncores):
            imin = min(chunks[ii])
            imax = max(chunks[ii])
            cmd = "python3 plotter.py %d %d" % (imin, imax)
            print(cmd)
            ps.append(Popen(cmd.split(" ")))

        while any([p.poll() is None for p in ps]):
            time.sleep(0.5)

        print("All done!")
