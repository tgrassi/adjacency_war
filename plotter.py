# this load pickels and plot
import pickle
import sys
from palette import palette

if len(sys.argv) != 3:
    sys.exit("ERROR: usage is ./%s imin imax" % sys.argv[0])

imin, imax = [int(x) for x in sys.argv[1:]]

for istep in range(imin, imax + 1):

    png_fname = "pngs_new/plot_%05d.png" % istep
    png_fname_zoom = "pngs_new/plot_zoom_%05d.png" % istep
    pickle_fname = "pickles/world_%05d.dat" % istep

    fp = open(pickle_fname, "rb")
    obj = pickle.load(fp)
    fp.close()

    # reassign palette
    for fidx, c in obj.countries.items():
        idx = obj.owners.index(c.owner)
        print(idx)
        obj.countries[fidx].color = palette[idx]

    print("plotting %s..." % png_fname)
    obj.plot(fname=png_fname)

    print("plotting %s..." % png_fname_zoom)
    obj.plot(fname=png_fname_zoom, zoom=True)

