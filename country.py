import matplotlib.pyplot as plt


# *****************
# *****************
# class Country
class Country:

    # *****************
    def __init__(self, fid, name, poly, adjs, color, owner):
        from random import random as rand
        self.name = name  # full name
        self.fid = fid  # FID (NUTS code)
        self.poly = poly  # polygon
        self.adjs = adjs  # adjacent FIDs
        self.color = self.color_org = color  # fill color
        self.owner = self.owner_org = owner  # owner

        # default status is idle
        self.status = "idle"

        # initialize data
        self.free = rand() * 0.2  # freedom probability (range 0-1)
        self.force = 0.5 + 0.5 * rand()  # attack force

    # ******************
    # return if country is conquered
    def is_conquered(self):
        return self.owner != self.owner_org

    # *****************
    # get border color depending on status
    def get_border_color(self):
        if self.status == "idle":
            return None
        elif self.status == "invaded":
            return "#ff0000"
        elif self.status == "conqueror":
            return None
        elif self.status == "free":
            return "#00ff00"
        else:
            import sys
            sys.exit("ERROR: unknown state %s in get_border_color" % self.status)

    # *****************
    # plot country polygon
    def plot(self):
        from matplotlib.patches import Polygon

        dd = self.poly
        geo = dd["geometry"]
        color = self.color
        color_border = self.get_border_color()
        ax = plt.gca()

        # hatching
        nh = 2
        h1 = "\\\\"*nh
        h2 = "//"*nh

        # draw polygon or multi polygon, depending on map type
        if geo["type"] == "Polygon":
            coo = geo["coordinates"][0]
            xs, ys = zip(*coo)
            plt.fill(xs, ys, color)

            if color_border is not None:
                ax.add_patch(Polygon(coo, fill=False, fc=color, ec="#000000", alpha=1,
                                     zorder=99, lw=1, hatch=h1))
                ax.add_patch(Polygon(coo, fill=False, fc=color, ec=color_border, alpha=1,
                                     zorder=99, lw=1, hatch=h2))
            else:
                ax.add_patch(Polygon(coo, fill=False, ec="#000000", alpha=1,
                                     zorder=99, lw=.3))


        elif geo["type"] == "MultiPolygon":
            for pol in geo["coordinates"]:
                xs, ys = zip(*pol[0])
                plt.fill(xs, ys, color)
                if color_border is not None:
                    ax.add_patch(Polygon(pol[0], fill=False, ec="#000000", alpha=1, zorder=99,
                                         lw=1, hatch=h1))
                    ax.add_patch(Polygon(pol[0], fill=False, ec=color_border, alpha=1, zorder=99,
                                         lw=1, hatch=h2))

                else:
                    ax.add_patch(Polygon(pol[0], fill=False, ec="#000000", alpha=1,
                                         zorder=99, lw=.3))

    # ********************
    # get country box limits, xmin, ymin, xmax, ymax
    def get_box(self):

        dd = self.poly
        geo = dd["geometry"]

        # store all x and y polygons points
        xall = []
        yall = []

        # polygon or multi polygon, depending on map type
        if geo["type"] == "Polygon":
            coo = geo["coordinates"][0]
            xs, ys = zip(*coo)
            xall += xs
            yall += ys

        elif geo["type"] == "MultiPolygon":
            for pol in geo["coordinates"]:
                xs, ys = zip(*pol[0])
                xall += xs
                yall += ys

        return min(xall), min(yall), max(xall), max(yall)

