import time
from selenium import webdriver


class Autopost:


    # **********************
    def __init__(self):
        login_data = []
        for row in open("fblogin.dat"):
            srow = row.strip()
            if srow == "" or srow.startswith("#"):
                continue
            login_data.append(srow)

        self.user, self.password = login_data

    # **********************
    def post_all(self, text_post, text_comment1, text_comment2, photo_post, photo_comment1):
        # GET STARTED
        browser = webdriver.Firefox()
        browser.get("http://www.facebook.com")


        # LOGIN
        username = browser.find_element_by_id("email")
        password = browser.find_element_by_id("pass")
        submit   = browser.find_element_by_id("loginbutton")
        username.send_keys(self.user)
        password.send_keys(self.password)
        submit.click()


        # CLICK LOAD PHOTO
        browser.get("https://mbasic.facebook.com/eurowarbot/");
        browser.find_element_by_name("view_photo").click()

        # LOAD PHOTO
        browser.find_element_by_name("file1").send_keys(photo_post)
        browser.find_element_by_name("add_photo_done").click()

        # WRITE POST MESSAGE
        browser.find_element_by_name("xc_message").send_keys(text_post)

        # SEND POST
        browser.find_element_by_name("view_post").click()

        # WAIT
        time.sleep(5)

        # CLICK FULL STORY TO ADD COMMENT 1 LATER
        browser.find_element_by_link_text("Full Story").click()

        # CLICK TO LOAD PHOTO IN COMMENT 1
        browser.find_element_by_name("view_photo").click()

        # WAIT
        time.sleep(5)

        # LOAD COMMENT 1 PHOTO
        browser.find_element_by_name("photo").send_keys(photo_comment1)

        # WRITE COMMENT 1
        browser.find_element_by_name("comment_text").send_keys(text_comment1)

        # SUBMIT COMMENT 1
        browser.find_element_by_xpath("//input[@value='Comment']").click()

        # WAIT
        time.sleep(5)

        # WRITE COMMENT 2
        browser.find_element_by_name("comment_text").send_keys(text_comment2)

        # SUBMIT COMMENT 2
        browser.find_element_by_xpath("//input[@value='Comment']").click()

        # WAIT
        time.sleep(5)

        # QUIT
        browser.quit()

