

# *****************
# *****************
class Weather:

    # *****************
    def __init__(self):
        self.rain = 0
        self.temperature = 20
        self.cloud = 0

        self.tmin = -10.
        self.tmax = 30.

    # *****************
    def get_string(self, udate=None):

        from datetime import datetime

        if udate is not None:
            doy = int(datetime.utcfromtimestamp(udate).strftime('%j'))
            self.compute_weather(doy)

        warm = [None, "pioggia leggera", "pioggia", "tempesta"]
        cold = [None, "neve leggera", "neve", "tormenta"]

        if self.temperature < 2:
            wstr = cold[self.rain]
        else:
            wstr = warm[self.rain]

        if self.rain == 0:
            if self.cloud == 0:
                wstr = "sereno"
            elif self.cloud == 1:
                wstr = "variabile"
            else:
                wstr = "coperto"

        return wstr + u", %d\u00B0C" % self.temperature

    # *****************
    def compute_weather(self, doy):
        from random import random as rand

        p = 0.5

        for ii in range(4):
            self.rain = ii
            if rand() < p:
                break

        self.cloud = round(rand() * 2)

        self.temperature = self.get_temperature(doy)

    # *****************
    def get_temperature(self, doy):
        import numpy as np
        from numpy.random import normal

        tref = (self.tmax - self.tmin) * np.sin(doy / 365. * np.pi)**2 + self.tmin

        temp = int(tref + normal(scale=5.))

        return max(min(temp, 45), -40)

