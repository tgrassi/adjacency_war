from SPARQLWrapper import SPARQLWrapper, JSON

qlist = {"Q228": "AD",
         "Q222": "AL",
         "Q40": "AT",
         "Q225": "BA",
         "Q219": "BG",
         "Q31": "BE",
         "Q39": "CH",
         "Q229": "CY",
         "Q213": "CZ",
         "Q183": "DE",
         "Q35": "DK",
         "Q191": "EE",
         "Q41": "EL",
         "Q29": "ES",
         "Q33": "FI",
         "Q142": "FR",
         "Q224": "HR",
         "Q28": "HU",
         "Q27": "IE",
         "Q38": "IT",
         "Q1246": "KX",
         "Q347": "LI",
         "Q37": "LT",
         "Q32": "LU",
         "Q211": "LV",
         "Q235": "MC",
         "Q236": "ME",
         "Q221": "MK",
         "Q233": "MT",
         "Q55": "NL",
         "Q20": "NO",
         "Q36": "PL",
         "Q45": "PT",
         "Q218": "RO",
         "Q403": "RS",
         "Q34": "SE",
         "Q215": "SI",
         "Q214": "SK",
         "Q238": "SM",
         "Q43": "TR",
         "Q145": "UK",
         "Q237": "VA"
         }

for k, v in qlist.iteritems():
    if v != "HU":
        continue
    print "query %s %s" % (k, v)

    # P734, surname
    # P735, name
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?name ?nameLabel ?count
        WITH {
          SELECT ?name (count(?person) AS ?count) WHERE {
            ?person wdt:P734 ?name .
            ?person wdt:P27 wd:#QCODE# .
          }
          GROUP BY ?name
          ORDER BY DESC(?count)
          LIMIT 300
        } AS %results
        WHERE {
          INCLUDE %results
          SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
        }
        ORDER BY DESC(?count)
    """

    query = query.replace("#QCODE#", k)
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()
    surnames = [x["nameLabel"]["value"] for x in res["results"]["bindings"]]
    fname = "%s.dat" % v
    fout = open(fname, "w")
    print len(surnames)
    for surname in sorted(surnames):
        fout.write("%s\n" % surname.encode('utf-8'))
    fout.close()
    print fname
