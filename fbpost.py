import facebook

# https://developers.facebook.com/tools/explorer/282360982692256?method=GET
# &path=me%3Ffields%3Did%2Cname%2Cassigned_pages&version=v3.2
# https://www.facebook.com/eurowarbot


# ******************
# ******************
class FBmanager:

    # ********************
    def __init__(self):
        self.access_token = "EAAEAzkoSYaABAP0DRLXcfSZCDcuhNLq9L7w7JN1J9BFV0XOFZBkCCc56IF7mJRiH38PTnhoNvlf7bouJNcxQwh3EMNVJbfxxUKGlSIMiUD3pN2BbbKZAtqTpK78oh34pN6FPrvujcTVZCV0ZBsX4P4BQGJmZA6eslxXqaxN3kWmgZDZD"
        self.page_id = "1559168687547784"

        app_id = "282360982692256"
        app_secret = "27d6d9b44f2cfcee0ad204902bb3f6d9"

        self.graph = facebook.GraphAPI(access_token=self.access_token, version="3.1")

    # ********************
    # post image and comment if not none
    def post_image(self, fname, message, fname_comments=None, message_comment=None,
                   message_second_comment=None):

        print(fname, message, fname_comments, message_comment, message_second_comment)

        print("posting image...")
        res = self.graph.put_photo(
            open(fname, 'rb').read(),
            message=message
        )

        post_id = res["post_id"]

        print("posting second comment to %s..." % post_id)
        if message_second_comment is not None:
            self.graph.put_comment("%s" % post_id, message=message_second_comment)

        print("posting comment to %s..." % post_id)
        if fname_comments is not None and message_comment is not None:
            self.graph.put_photo(open(fname_comments, 'rb').read(),
                                 "/%s/comments" % post_id,
                                 message=message_comment
                                 )

        print("posting done")

    # ********************
    # simple post
    def post(self, message):

        return self.graph.put_object(
            self.page_id,
            "feed",
            message=message
        )
