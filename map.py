import json
import os
from pprint import pprint
import matplotlib.pyplot as plt
import matplotlib
from random import shuffle
from random import random as rand
import numpy as np

# default palette
palette = ["#89C5DA", "#DA5724", "#74D944", "#CE50CA", "#3F4921", "#C0717C", "#CBD588", "#5F7FC7",
    "#673770", "#D3D93E", "#38333E", "#508578", "#D7C1B1", "#689030", "#AD6F3B", "#CD9BCD",
    "#D14285", "#6DDE88", "#652926", "#7FDCC0", "#C84248", "#8569D5", "#5E738F", "#D1A33D",
    "#8A7C64", "#599861"]

# islands will be ignored
islands = ["The Bahamas", "Bermuda", "Cuba", "Fiji", "Iceland", "Jamaica", "Madgascar", "Malta", "New Zealand", "Puerto Rico", "Vanuatu", "Dominican Republic", "Haiti"]

# name of months
months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dec"]
#months = ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"]

# load adjacency
with open('country_adj_fullname.json') as f:
    adj = json.load(f)

# load map polygons
with open('countries.geo.json') as f:
    data = json.load(f)

# file where to store initial data
fname_init = 'data_init.json'

# if file with initial data do not exist create initial data
if not os.path.isfile(fname_init):

    # loop on countries to create a dictionary
    countries = dict()
    for ii, dd in enumerate(data["features"]):
        name = dd["properties"]["name"]
        iso = dd["id"]
        # ignore countries not in adjacency map, or in island
        if name not in adj or name in islands:
            print("skip", name)
            continue

        # initialize countries
        countries[name] = {"aggressive": rand(),  # aggressivity (range 0-1)
            "owner": name,  # at init owner is itself
            "color": ii,  # owner color
            "color_org": ii,  # initial color
            "done": False,  # turn is done
            "free": rand() * 0.2,  # freedom probability (range 0-1)
            "force": rand() * len(adj[name])}  # attack force (range 0-inf)

    # store init data to file
    with open(fname_init, 'w') as outfile:
        json.dump(countries, outfile)

else:
    # load initial data when init file found
    print("loading from %s" % fname_init)
    with open(fname_init) as f:
        countries = json.load(f)


# remove countries from adjacency that are not in countries dictionary
adj = {k: [x for x in v if x in countries] for k, v in adj.iteritems()}

# get score dictionary, key: country name, value: total force
def get_score():
    # compute score for each coutry by summing force of owned lands
    score = {k: 0 for k in countries}
    for name in countries:
        owner = countries[name]["owner"]
        score[owner] += countries[name]["force"]
    return score

# plot map png
def plot_map(istep):
    plt.clf()

    # get total score dictionary
    score = get_score()

    # total score
    score_tot = sum(score.values())

    # get keys (country names) sorted descending
    rank = sorted(score, key=score.get)[::-1]

    # create a geodata dictionary
    geodata = dict()
    for dd in data["features"]:
        name = dd["properties"]["name"]
        geodata[name] = dd

    # loop on countries ranked
    for name in rank:
        dd = geodata[name]
        name = dd["properties"]["name"]
        iso = dd["id"]
        # this should be useless
        if name not in adj or name in islands:
            #print name
            continue

        # store some data locally
        geo = dd["geometry"]
        cl = countries[name]["color"]
        color = palette[cl % len(palette)]

        # draw polygon or multi polygon, depending on map type
        if geo["type"] == "Polygon":
            coo = geo["coordinates"][0]
            xs, ys = zip(*coo)
            plt.fill(xs, ys, color)
        elif geo["type"] == "MultiPolygon":
            for pol in geo["coordinates"]:
                xs, ys = zip(*pol[0])
                plt.fill(xs, ys, color)

        # put first ranked into legend
        if name in rank[:5] and score[name] > 0:
            plt.scatter([0], [0], marker="s", c=color, label="%s %.1f" % (name, score[name]*100/score_tot))

    # plot titles and save
    plt.title("Year %d %s" % (2020 + istep/12, months[istep % 12]))
    plt.legend(loc="lower left", fontsize=8)
    plt.axes().set_aspect(1.5)
    plt.savefig("pngs/plot_%.5d.png" % istep, dpi=180)


# plot map png
def plot_map_quantity(qty):
    plt.clf()

    # create a geodata dictionary and store all quantity value
    geodata = dict()
    qty_all = []
    for dd in data["features"]:
        name = dd["properties"]["name"]
        geodata[name] = dd
        if name not in adj or name in islands:
            continue
        qty_all.append(countries[name][qty])

    # get min/max of given quantity
    qty_min = min(qty_all)
    qty_max = max(qty_all)

    # loop on countries ranked
    for name in geodata:
        dd = geodata[name]
        name = dd["properties"]["name"]
        iso = dd["id"]

        # skip islands and countries not in adjacency map
        if name not in adj or name in islands:
            continue

        # store some data locally
        geo = dd["geometry"]
        cmap = matplotlib.cm.get_cmap('RdYlGn')

        # get hex from rgb palette
        rgb = cmap(float(countries[name][qty] - qty_min) / (qty_max - qty_min))
        color = matplotlib.colors.rgb2hex(rgb[:3])

        # draw polygon or multi polygon, depending on map type
        if geo["type"] == "Polygon":
            coo = geo["coordinates"][0]
            xs, ys = zip(*coo)
            plt.fill(xs, ys, color)
        elif geo["type"] == "MultiPolygon":
            for pol in geo["coordinates"]:
                xs, ys = zip(*pol[0])
                plt.fill(xs, ys, color)

    # define a mappable for colorbar
    norm = matplotlib.colors.Normalize(vmin=qty_min, vmax=qty_max)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    # plot titles and save
    plt.title(qty)
    plt.colorbar(sm, orientation="horizontal")
    plt.savefig("pngs/plot_%s.png" % qty, dpi=180)

from copy import copy, deepcopy

# advance a game step
def step():

    # set turn done to false
    for name, cd in countries.iteritems():
        countries[name]["done"] = False

    # shuffle turns
    names = countries.keys()
    shuffle(names)

    # loo on turns
    for name in names:

        # get country data
        cd = countries[name]

        # country owned by others cannot do anything
        if cd["owner"] != name:
            continue

        # country already done turn cannto do anything else
        if cd["done"]:
            continue

        # skip depending on aggressivity
        if rand() > cd["aggressive"]:
            continue

        # copy adjacents
        neis = copy(adj[name])
        force = 1
        # add all the adjacent countries and compute total force
        for name2, cd2 in countries.iteritems():
            if cd2["owner"] == name:
                force += cd2["force"]
                neis += adj[name2]

        found = False
        # search for attackable enemy (100 attempts)
        for _ in range(100):
            shuffle(neis)
            enemy = neis[0]
            # enemy can be only not owned country that still wait its turn
            if countries[enemy]["owner"] != name and not countries[enemy]["done"]:
                found = True
                break

        force_enemy = 1
        # compute enemy force by summing lands
        for name2, cd2 in countries.iteritems():
            if cd2["owner"] == enemy:
                force_enemy += cd2["force"]

        # dice to determine winner based on force
        if rand() < force / (force + force_enemy) and found:
            owner = countries[enemy]["owner"]
            # replace owner and colors
            countries[enemy]["owner"] = name
            countries[enemy]["color"] = cd["color"]
            countries[owner]["done"] = True

            if enemy != owner:
                print "%s conquista il territorio di %s appartenente a %s" % (name, enemy, owner)
            else:
                print "%s conquista l'impero di %s. I territori di %s:" % (name, owner, owner)

            # update all the countries owned by enemy
            for name2, cd2 in countries.iteritems():
                if cd2["owner"] == enemy:
                    if cd2["free"] > rand():
                        print " - %s ritorna indipendente" % name2
                        countries[name2]["owner"] = name2
                        countries[name2]["color"] = countries[name2]["color_org"]
                        countries[name2]["done"] = True
                    else:
                        print " - %s acquisito da %s" % (name2, name)
                        countries[name2]["owner"] = name
                        countries[name2]["color"] = cd["color"]
                        countries[name2]["done"] = True

        # enemy has ended turn
        if found:
            countries[enemy]["done"] = True

        # this country ended turn
        countries[name]["done"] = True


# plot initial map
plot_map(0)
# plot maps of various countries quantities
plot_map_quantity("force")
plot_map_quantity("aggressive")
plot_map_quantity("free")

# init structure where to store the countries evolution
countries_history = []

# loop on steps for a max number of iterations
# loop is broken when a country reach 100%
for istep in range(100000):

    # some ouptput to screen
    print "****************", istep + 1

    # advance for a step
    step()

    # get score ditionary to get maximum score
    score = get_score()
    score_tot = sum(score.values())
    rank = sorted(score, key=score.get)[::-1]
    max_score = score[rank[0]] * 100 / score_tot

    # store countries data into history to be plotted later
    countries_history.append(deepcopy(countries))

    # break when we have a winner
    if max_score > 99.9:
        break

# loop to plot into pngs folder
for istep, countries in enumerate(countries_history):
    print "map", istep
    plot_map(istep+1)


