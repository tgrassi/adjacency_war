import matplotlib.pyplot as plt
from country import Country


# *****************
# *****************
# class World does everything
class World:

    # *****************
    def __init__(self):
        from datetime import datetime
        from palette import palette
        from language import Language
        from general import General
        import time

        self.history = []

        # check if war respect some outcome criteria, e.g. who wins
        self.is_good_war = True

        # free probability per step
        self.pfree = 0.01

        # battle probability limits
        self.pbattle_min = 0.4
        self.pbattle_max = 0.6

        # store current time
        yr = datetime.today().year + 1
        dt = datetime(yr, 1, 1)
        self.time = time.mktime(dt.timetuple())

        # string with last event
        self.last_event_message = ""
        self.last_event_image = None
        self.last_event_image_zoom = None

        # load units name
        self.units = self.load_units()

        # language object for names
        self.language = Language()

        # polygons
        polys = self.load_polys()

        # adjacency map
        self.adjmap = self.load_adjmap()

        # init countries
        self.owners = []
        self.countries = dict()
        self.generals = dict()
        self.best_general = None
        for fid, data in polys.items():
            owner = str(data["properties"]["CNTR_CODE"])
            if owner not in self.owners:
                self.owners.append(owner)
                name = self.language.get_name(owner)
                self.generals[owner] = General(name, owner)
            color = palette[self.owners.index(owner) % len(palette)]

            name = data["properties"]["NUTS_NAME"].split("(")[0].strip().title()
            self.countries[fid] = Country(fid, name, data, self.adjmap[fid], color, owner)

        # check number of palettes
        if len(self.owners) > len(palette):
            print("WARNING: %d owners are more than %d palettes" % (len(self.owners), len(palette)))

        # normalize force to 100%
        self.normalize()

    # *****************
    # normalize force
    def normalize(self, normalization=1e2):
        tot_force = 0e0
        for c in self.countries.values():
            tot_force += c.force

        for c in self.countries.values():
            c.force = c.force / tot_force * normalization

    # *****************
    # print best generals to screen
    def print_best_generals_rank(self, best=999999):
        print(self.get_best_generals_text(best=best))

        print(self.get_best_general_text())

    # *****************
    # get best generals rank
    def get_best_generals_text(self, best=99999):
        rank = []
        # loop to prepare data
        for country, general in self.generals.items():
            row = "%s %s %d" % (self.owner2emoji(country), general.name, general.win)
            rank.append([general.win, row])

        # sort generals by wins
        rows_sort = sorted(rank, key=lambda x: x[0], reverse=True)

        # print generals with wins > 0
        rows = ""
        for ii, row in enumerate(rows_sort):
            if ii >= best:
                break
            if row[0] > 0:
                rows += row[1] + "\n"

        return rows

    # ****************
    # best dead general
    def get_best_general_text(self):
        general = self.best_general
        if general is None:
            return None
        if general.win <= 0:
            return None
        country = general.owner
        return "%s %s %d" % (self.owner2emoji(country), general.name, general.win)

    # *****************
    def save_step(self, fname):
        import json

        data = {"message": self.get_post_message(),
                "comment": self.get_post_comment(),
                "second_comment": self.get_post_second_comment(),
                "plot": self.last_event_image,
                "plot_zoom": self.last_event_image_zoom}

        with open(fname, 'w') as outfile:
            json.dump(data, outfile)

        print("post data saved to %s" % fname)

    # *****************
    # load adjacency map from file
    # (dictionary with NUTS codes)
    @staticmethod
    def load_adjmap(fname="nuts/adj.json"):
        import json
        # load adjacency
        with open(fname) as f:
            adjmap = json.load(f)

        return adjmap

    # *****************
    # load polygons from file
    @staticmethod
    def load_polys(fname="nuts/merged_plus.geojson"):
        import json
        # load map polygons
        with open(fname) as f:
            polys = json.load(f)["features"]

        return {str(dd["properties"]["FID"]): dd for dd in polys}

    # ******************
    @staticmethod
    def load_units(fname="data/units.dat"):
        units = []
        for row in open(fname):
            srow = row.strip().lower()
            if srow == "":
                continue
            if srow.startswith("#"):
                continue
            units.append(srow)
        return units

    # *********************
    # get box size containing non-idling countries
    @staticmethod
    def get_action_box(countries, whats=None):

        if whats is None:
            whats = ["invaded", "free", "conqueror"]

        xmin = ymin = 1e99
        xmax = ymax = -1e99
        for country in countries:
            if country.status in whats:
                box = country.get_box()
                xmin = min(box[0], xmin)
                ymin = min(box[1], ymin)
                xmax = max(box[2], xmax)
                ymax = max(box[3], ymax)

        return xmin, ymin, xmax, ymax

    # *****************
    # plot world map
    def plot(self, fname=None, dpi=180, zoom=False, png_title=False):
        self.plot_map(self.countries.values(),
                      fname=fname,
                      dpi=dpi,
                      zoom=zoom,
                      png_title=png_title)

    # *****************
    # plot world map
    def plot_map(self, countries, fname=None, dpi=180, zoom=False, png_title=False):
        from matplotlib.patches import Ellipse, Rectangle

        plt.clf()

        # loop on countries to plot polygons
        for c in countries:
            c.plot()

        # remove axes
        plt.axis('off')

        # if zoom limits
        if zoom:
            # get box active limits
            xmin, ymin, xmax, ymax = self.get_action_box(countries)
            # box size
            dx = (xmax - xmin) / 2
            dy = (ymax - ymin) / 2
            # get largest box size
            dd = 2 * max(dx, dy)
            # box center
            xc = (xmax + xmin) / 2
            yc = (ymax + ymin) / 2

            # set image limits
            plt.xlim(xc - dd, xc + dd)
            plt.ylim(yc - dd, yc + dd)

        # add title
        if png_title:
            title = part = ""
            for word in self.last_event_message.split(" "):
                part += word + " "
                if len(part) > 50:
                    title += part.strip() + "\n"
                    part = ""
            title += part
            plt.title(title.strip(), fontsize=6)

        # tight layout
        #plt.tight_layout()

        # set aspect ratio
        plt.axes().set_aspect(1.5)

        if not zoom:
            plt.xlim(-10.5, 44.5)
            plt.ylim(34.5, 71.5)
            pad = -0.15

            xmin, ymin, xmax, ymax = self.get_action_box(countries, whats=["invaded", "free"])
            xc = (xmax + xmin) / 2
            yc = (ymax + ymin) / 2

            circle1 = Ellipse((xc, yc), 15, 10, fill=False, ec="w", lw=2)
            circle2 = Ellipse((xc, yc), 15, 10, fill=False, ec="k", ls=":", lw=2)
            plt.axes().add_artist(circle1)
            plt.axes().add_artist(circle2)
        else:
            pad = -.35

        if not zoom:
            self.plot_legend()

        # if fname is missing show to screen, else save to fname
        if fname is None:
            plt.show()
        else:
            print("Plotting %s..." % fname)

            # store last image name
            if zoom:
                self.last_event_image_zoom = fname
            else:
                self.last_event_image = fname

            plt.savefig(fname, dpi=dpi, bbox_inches='tight', pad_inches=pad, facecolor="#cddce0")

        plt.close("all")

    # ******************
    def plot_legend(self):
        from matplotlib.patches import Rectangle
        import numpy as np

        owners = []
        colors = []
        for c in self.countries.values():
            if c.owner in owners:
                continue
            owners.append(c.owner)
            colors.append(c.color)

        for i, idx in enumerate(np.argsort(owners)[::-1]):
            ypos = 42.5 + i * 0.6
            plt.text(36, ypos, owners[idx], fontsize=4)

            rect = Rectangle((34, ypos), 1., 0.5, linewidth=0.2, edgecolor='k', facecolor=colors[idx])

            # Add the patch to the Axes
            plt.gca().add_patch(rect)

    # ******************
    def get_post_message(self):
        message = self.get_date()
        message += "\n" + self.last_event_message
        return message

    # **********************
    def get_post_comment(self):
        comment = "Rank per forza totale (%):\n"
        comment += self.get_rank_text()
        return comment

    # **********************
    def get_post_second_comment(self):
        comment = "Rank comandanti per vittorie:\n"
        comment += self.get_best_generals_text()
        comment += "\n"
        best_general = self.get_best_general_text()
        if best_general is not None:
            comment += "Miglior generale deceduto: %s" % best_general
        return comment

    # ************************
    # post last step to facebook
    def post_last_step(self):
        from fbpost import FBmanager

        fb = FBmanager()

        # message with the action
        message = self.get_post_message()

        # countries rank for the comments
        comment = self.get_post_comment()

        # generals rank
        second_comment = self.get_post_second_comment()

        print("posting to fb...")

        # do posting
        fb.post_image(self.last_event_image,
                      message,
                      fname_comments=self.last_event_image_zoom,
                      message_comment=comment,
                      message_second_comment=second_comment)

    # ************************
    @staticmethod
    def post_history_step(data):
        from autopost import Autopost
        from datetime import datetime
        import os

        ap = Autopost()

        print("current time: %s" % datetime.now())
        print("posting history step to fb...")

        path = os.getcwd()

        # do posting
        ap.post_all(data["message"],
                    data["comment"],
                    data["second_comment"],
                    path + "/" + data["plot"],
                    path + "/" + data["plot_zoom"])


    # ************************
    @staticmethod
    def post_history_step_old(data):
        from fbpost import FBmanager
        from datetime import datetime

        fb = FBmanager()

        print("current time: %s" % datetime.now())
        print("posting history step to fb...")

        # do posting
        fb.post_image(data["plot"],
                      data["message"],
                      data["plot_zoom"],
                      data["comment"],
                      data["second_comment"])

    # ************************
    # delay to the next post in seconds
    @staticmethod
    def delay_next_post(hmin=8, hmax=23, force=None):
        import datetime
        import time

        # force a sleep in seconds
        if force is not None:
            print("%.2f minute(s) of sleep before the next step..." % (force / 60.))
            time.sleep(force)
            return

        now = datetime.datetime.now()
        h = now.hour
        if hmin <= h <= hmax:
            delay = 1
        else:
            delay = min(5, hmin - h)

        for i in range(delay):
            print("%d hour(s) of sleep before the next step..." % (delay - i))
            time.sleep(3600)

    # ***************
    # get simulation date in string format
    def get_date(self):
        from datetime import datetime

        date = datetime.utcfromtimestamp(self.time).strftime('%-d %B %Y')

        months = {
            "january": "gennaio",
            "february": "febbraio",
            "march": "marzo",
            "april": "aprile",
            "may": "maggio",
            "june": "giugno",
            "july": "luglio",
            "august": "agosto",
            "september": "settembre",
            "october": "ottobre",
            "november": "novemebre",
            "december": "dicembre",
        }

        for m_eng, m_ita in months.items():
            date = date.lower().replace(m_eng, m_ita.title())

        return date

    # *******************
    @staticmethod
    def put_prop_article(name):

        if name.lower() in ["cipro", "san marino", "montecarlo", "malta"]:
            return "di %s" % name

        if name[0].lower() in list("aeiou"):
            return "dell'%s" % name

        if name.endswith("a"):
            return "della %s" % name
        elif name.endswith("o"):
            return "del %s" % name

    # ************************
    # nation id to name
    @staticmethod
    def owner2name(owner):
        cdic = {
            "AD": "Andorra",
            "AL": "Albania",
            "AT": "Austria",
            "BA": "Bosnia Erzegovina",
            "BG": "Bulgaria",
            "BE": "Belgio",
            "CH": "Svizzera",
            "CY": "Cipro",
            "CZ": "Repubblica Ceca",
            "DE": "Germania",
            "DK": "Danimarca",
            "EE": "Estonia",
            "EL": "Grecia",
            "ES": "Spagna",
            "FI": "Finlandia",
            "FR": "Francia",
            "HR": "Croazia",
            "HU": "Ungheria",
            "IE": "Irlanda",
            "IT": "Italia",
            "KX": "Kosovo",
            "LI": "Liechtenstein",
            "LT": "Lituania",
            "LU": "Lussemburgo",
            "LV": "Lettonia",
            "MC": "Montecarlo",
            "ME": "Montenegro",
            "MK": "Macedonia",
            "MT": "Malta",
            "NL": "Olanda",
            "NO": "Norvegia",
            "PL": "Polonia",
            "PT": "Portogallo",
            "RO": "Romania",
            "RS": "Serbia",
            "SE": "Svezia",
            "SI": "Slovenia",
            "SK": "Slovacchia",
            "SM": "San Marino",
            "TR": "Turchia",
            "UK": "Regno Unito",
            "VA": "Vaticano"
        }
        return cdic[owner]

    # *****************
    # get flag emoji from name
    @staticmethod
    def owner2emoji(owner):
        import flag

        # replace countries
        reps = {"UK": "GB",
                "EL": "GR",
                "KX": "XK"}

        if owner in reps:
            owner = reps[owner]

        return flag.flagize(":%s:" % owner)

    # *****************
    # get score dictionary
    def get_score(self):

        score = dict()
        for owner in self.owners:
            score[owner] = self.get_total_force(owner)
        return score

    # ******************
    # get ranked owners, e.g. ["IT", "UK", "DE", ...]
    def get_rank(self):

        score = self.get_score()
        return sorted(score, key=score.get)[::-1]

    # ****************
    # get max score, percent
    def get_max_score(self):
        score = self.get_score().values()
        return max(score) * 1e2 / sum(score)

    # ****************
    # get text rank in percent
    def get_rank_text(self, best=99999):

        score = self.get_score()
        rank = self.get_rank()

        # number of countries to print
        best = min(best, len(rank))

        # sum total score
        tot_score = sum(score.values())
        # print rank to screen

        rank_text = ""
        for owner in rank[:best]:
            if score[owner] <= 0e0:
                continue
            rank_text += "%s %s %.2f\n" % (self.owner2emoji(owner),
                                           self.owner2name(owner).ljust(30),
                                           score[owner] * 1e2 / tot_score)
        return rank_text

    # ****************
    # print rank to screen
    def print_rank(self, best=99999):
        print(self.get_rank_text(best))

    # *****************
    # get total force of country
    def get_total_force(self, country):

        # if country is a string use it, otherwise assumed to be country object
        if type(country) == str:
            owner = country
        else:
            owner = country.owner

        # sum force of countries owned by name
        force = 0e0
        for k, v in self.countries.items():
            if v.owner == owner:
                force += v.force

        return force

    # *****************
    # get list of objects of owned countries
    def get_owned(self, country):

        # if country is a string use it, otherwise assumed to be country object
        if type(country) == str:
            owner = country
        else:
            owner = country.owner

        owned = []
        for k, v in self.countries.items():
            if v.owner == owner:
                owned.append(v)

        return owned

    # ****************
    # advance simulation by a step
    def step(self):
        from random import random as rand

        # set all counties status to idle
        for fid, country in self.countries.items():
            country.status = "idle"

        # increase time by one day
        self.time += 3600 * 24

        # throw a dice
        dice = rand()

        free_found = False
        # if dice then search free country
        if dice < self.pfree:
            free_found = self.free()

        # if dice and free not found then start battle
        if dice >= self.pfree or not free_found:
            self.battle()

    # *****************
    # check if some country gets independent
    def free(self):
        from random import random as rand

        # randomize keys to loop on countries
        fids = self.countries.keys()
        for fid in fids:

            # get player country
            player = self.countries[fid]

            # check if free
            if player.free < rand() and player.is_conquered():

                # store message to screen
                self.last_event_message = "INSURREZIONE! %s (%s %s) ritorna a %s %s"\
                                          % (player.name,
                                             self.owner2emoji(player.owner),
                                             self.owner2name(player.owner),
                                             self.owner2emoji(player.owner_org),
                                             self.owner2name(player.owner_org))

                print(self.last_event_message)

                # restore original color and owner
                player.owner = player.owner_org
                player.color = player.color_org
                player.status = "free"

                return True

        return False

    # *****************
    # do battle
    def battle(self):
        from random import shuffle
        from random import random as rand
        from general import General
        from weather import Weather
        from copy import copy
        import sys

        # enemy found flag
        found = False

        # default objects to trig error
        player = enemy = None

        # randomize keys to loop on countries
        fids = list(self.countries.keys())
        shuffle(fids)
        for fid in fids:

            # get player country
            player = self.countries[fid]

            # get adjacent countries
            adjs = player.adjs
            shuffle(adjs)

            # loop on adjacent countries
            for adj in adjs:
                # get enemy and check if not owned
                enemy = self.countries[adj]
                # break when enemy found
                if enemy.owner != player.owner:
                    found = True
                    break

            # break loop on owned when enemy found
            if found:
                break

        # rise error if enemy not found
        if not found:
            print("ERROR: enemy not found")
            sys.exit()

        # if enemy found start battle
        if found:

            # get total force
            force_player = self.get_total_force(player)
            force_enemy = self.get_total_force(enemy)

            # winning probability for player
            pbattle = force_player / (force_player + force_enemy)

            # limiting winning probability
            pbattle = min(pbattle, self.pbattle_max)
            pbattle = max(pbattle, self.pbattle_min)

            # check if successful invasion otherwise lost
            if rand() < pbattle:
                conqueror = player
                invaded = enemy
            else:
                conqueror = enemy
                invaded = player

            # set player to conqueror for edge colors
            conqueror.status = "conqueror"

            # get unit names
            shuffle(self.units)
            conqueror_units = self.units[0]
            invaded_units = self.units[1]

            # if reconquered change verb
            if conqueror_units[:2].lower() in ["l'", "la"]:
                verbs = ["conquista", "invade", "annette", "occupa",
                         "assoggetta", "sottomette", "espugna"]
                shuffle(verbs)
                verb = verbs[0]
            else:
                verbs = ["conquistano", "annettono", "occupano",
                         "assoggettano", "sottomettono", "espugnano"]
                shuffle(verbs)
                verb = verbs[0]

            if conqueror.owner == invaded.owner_org:
                verb = "ri" + verb

            # store message
            self.last_event_message = \
                "- %s %s %s %s %s sconfiggendo %s %s %s!" % (
                    conqueror_units.capitalize(),
                    self.put_prop_article(self.owner2name(conqueror.owner)),
                    self.owner2emoji(conqueror.owner),
                    verb,
                    invaded.name,
                    invaded_units,
                    self.put_prop_article(self.owner2name(invaded.owner)),
                    self.owner2emoji(invaded.owner))

            # add wins if greater than zero
            if self.generals[invaded.owner].win > 1:
                wins_message = "Dopo %d vittorie consecutive lo sconfitto" \
                               % self.generals[invaded.owner].win
            else:
                wins_message = "Lo sconfitto"

            # conquered general message
            self.last_event_message += "\n- %s %s %s adesso e' diventato %s." \
                                       % (wins_message,
                                          self.generals[invaded.owner].name,
                                          self.owner2emoji(invaded.owner),
                                          self.language.get_job())

            # set general if best general not set
            if self.best_general is None:
                self.best_general = copy(self.generals[invaded.owner])

            # replace if found a better best general
            if self.best_general.win < self.generals[invaded.owner].win:
                self.best_general = copy(self.generals[invaded.owner])

            # create new general when conquered
            name = self.language.get_name(invaded.owner)
            self.generals[invaded.owner] = General(name, invaded.owner)

            # increase wins number
            self.generals[conqueror.owner].win += 1

            # write general stats if more than a win
            if self.generals[conqueror.owner].win > 1:
                self.last_event_message += "\n- %d vittorie consecutive per il %s %s." \
                                           % (self.generals[conqueror.owner].win,
                                              self.generals[conqueror.owner].name,
                                              self.owner2emoji(conqueror.owner))
            else:
                self.last_event_message += "\n- Prima vittoria per il %s %s." \
                                           % (self.generals[conqueror.owner].name,
                                              self.owner2emoji(conqueror.owner))

            # check if completely gone
            enemy_owned = len(self.get_owned(invaded))
            if enemy_owned - 1 == 0:
                self.last_event_message += "\n- %s %s completamente sconfitta." \
                                           % (self.owner2emoji(invaded.owner),
                                              self.owner2name(invaded.owner))

            w = Weather()
            self.last_event_message += "\n- Meteo: " + w.get_string(self.time)
            self.last_event_message += "\n\n (altri dettagli sulla battaglia nei commenti)"

            print(self.last_event_message)

            # change enemy properties
            invaded.owner = conqueror.owner
            invaded.color = conqueror.color
            invaded.status = "invaded"

    # ******************
    # loop to do battles until single winner
    def do_war(self, plot=False, save=True, post=False, steps=None, rank=5,
               png_title=False, delay=300, rank_generals=5, pickles=True):

        import pickle

        istep = 0

        # list of countries ranked #1 during war
        rank_first_countries = []

        # loop forever until 100% conquered
        while True:
            print("***** %d *****" % (istep + 1))

            self.step()
            if rank > 0:
                self.print_rank(best=rank)
                # store country #1 for statistics at the end
                this_rank = self.get_rank()
                rank_first_countries.append(this_rank[0])

            if rank_generals > 0:
                self.print_best_generals_rank(best=rank_generals)

            # get max score (percent)
            max_score = self.get_max_score()

            self.last_event_image = "pngs/plot_%05d.png" % istep
            self.last_event_image_zoom = "pngs/plot_zoom_%05d.png" % istep

            # plot pngs
            if plot:
                # plot full map
                self.plot(self.last_event_image, png_title=png_title)

                # plot zoomed map
                self.plot(self.last_event_image_zoom, zoom=True, png_title=png_title)

            # save step
            if save:
                self.save_step("history/post_%05d.dat" % istep)

            # post to facebook
            if post:
                # post to facebook page
                self.post_last_step()

                # wait for the next post
                self.delay_next_post(force=delay)

            # save object to pickles
            if pickles:
                fp = open("pickles/world_%05d.dat" % istep, "wb")
                pickle.dump(self, fp)
                fp.close()

            # when 100% ends
            if max_score >= 99.99999999999999:
                break

            # break after given steps if set
            if steps is not None:
                if steps == istep + 1:
                    break

            # increase step count
            istep += 1


        # print statistics on how many times country has been first
        self.print_count_first_rank(rank_first_countries)

        self.is_good_war = self.evaluate_count_first_rank(rank_first_countries)

    # **********************
    def evaluate_count_first_rank(self, rank_first_countries):

        uniq = list(set(rank_first_countries))

        if len(uniq) < 4:
            return False

        winner = self.owner2name(rank_first_countries[-1])
        if winner in ["Germania", "Ungheria"]:
            return False

        return True

    # **********************
    def print_count_first_rank(self, rank_first_countries):

        uniq = list(set(rank_first_countries))

        print("***********************")
        print("Times country is first:")
        for owner in uniq:
            name = self.owner2name(owner)
            print(name, rank_first_countries.count(owner))

    # ********************
    def clean(self):
        from glob import glob
        import os

        for fname in glob("history/*dat"):
            os.remove(fname)
        for fname in glob("pickles/*dat"):
            os.remove(fname)
        for fname in glob("pngs/*png"):
            os.remove(fname)


    # ****************
    def plot_history(self):
        for hist in self.history:
            countries = hist["countries"]
            fname = hist["png_file"]
            fname_zoom = hist["png_file_zoom"]
            self.plot_map(countries, fname=fname, dpi=180, zoom=False)
            self.plot_map(countries, fname=fname_zoom, dpi=180, zoom=False)

    # ***********************
    def post_history(self, delay=3600, restart_step=0):
        import glob
        import json

        files = sorted(glob.glob("history/post_*.dat"))

        nfiles = len(files)

        for ii, fname in enumerate(files):
            if ii < restart_step - 1:
                continue
            with open(fname) as f:
                data = json.load(f)

            print("posting %d of %d..." % (ii + 1, nfiles))

            self.post_history_step(data)

            self.delay_next_post(force=delay)
